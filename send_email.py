#! /usr/bin/python
# Written By Tom Paulus, @tompaulus, www.tompaulus.com

import json
import os
import sys
from datetime import datetime

import pytz
import requests


class SendGrid(object):
    def __init__(self, api_key):
        self.key = api_key

    def send(self, to, to_name, subject, message_html):
        data = dict()
        data['to'] = to
        data['toname'] = to_name
        data['from'] = 'python@whitestarsystems.com'  # Replace this with your from address
        data['fromname'] = 'Python'
        data['subject'] = subject
        data['html'] = message_html

        session = requests.Session()
        session.headers.update({'Authorization': 'Bearer ' + self.key})
        sg_response = session.post('https://api.sendgrid.com/api/mail.send.json', data)

        json = sg_response.json()

        if json['message'] == 'success':
            return True, None
        else:
            return False, json['errors']


def read_api_keys(rel_file_path="./API.json"):
    """
    :param rel_file_path: Location of the API Key File relative to the file being executed.
    :return sendgrid_user: FancyHands API Key
    :return sendgrid_pass: FancyHands API Secret
    :rtype : tuple

    """
    sendgrid_user = ''
    sendgrid_pass = ''

    try:
        with open(rel_file_path) as license_file:
            line = license_file.readline()

            while line != '':
                if line.count('User:') == 1:
                    # Key Line
                    sendgrid_user = line[6: -1]

                elif line.count('Pass:') == 1:
                    # Secret Line
                    sendgrid_pass = line[6: -1]

                line = license_file.readline()  # Advance to the next line in the file.

    except IOError:
        print 'The API Key file does not exist. Not Fatal.'
        return None, None

    return sendgrid_user, sendgrid_pass


def read_pipe():
    data_in = sys.stdin.read()
    return data_in


def make_email(error, date):
    with open('./email_template.html') as template:
        msg = template.read().replace('*|Error Message|*', error).replace('*|Date|*', date)

    return msg


if __name__ == "__main__":
    os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))

    d = datetime.now(tz=pytz.timezone('US/Pacific')).strftime('%A, %B %d, %Y')
    e = read_pipe()
    # e = """
    # Traceback (most recent call last):
    #   File "test.py", line 7, in <module>
    #     l[3]  # List Index out of Range Error
    # IndexError: list index out of range
    #
    #     """

    e = e.strip()

    credentials_dict = json.loads(open('./API.json').read())
    # Uncomment the lines below if you want to bypass the external file.
    # username = ''
    # password = ''

    if e != '':
        response, e = SendGrid(credentials_dict['SendGrid']['api_key']).send(
            'tom@tompaulus.com', 'Tom Paulus', 'Fatal Script Error', make_email(e, d))

        if not response:
            print e
